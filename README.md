# patrimonium_editor

patrimonium_editor. A virtual Research Environment for Historians.

This eXist-db application is a front-end for using the [ausoHNum-library](https://gitlab.huma-num.fr/estudium/ausohnum-library). A 3rd eXist-db app must also be installed to store data: [eStudiumData](https://gitlab.huma-num.fr/patrimonium_editor/patrimonium_editordata)

## Getting started

Once you've cloned this repository, you may run ant and build a .xar file to be uploaded to your exist-db server.


## License

GPL v3 - [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.html)
