xquery version "3.1";

import module namespace config="https://ausohnum.huma-num.fr/apps/patrimonium_editor/config" at "../config.xqm";

import module namespace ausohnumCommons="http://ausonius.huma-num.fr/commons"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/commons/commonsApp.xql";

import module namespace functx="http://www.functx.com";

import module namespace spatiumStructor="http://ausonius.huma-num.fr/spatiumStructor"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/spatiumStructor/spatiumStructor.xql";

import module namespace placeRecordGenerator ="http://ausonius.huma-num.fr/placeRecordGenerator"
      at "./placeRecordGenerator.xql";
      
declare namespace apc = "http://patrimonium.huma-num.fr/onto#";
declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace ev = "http://www.w3.org/2001/xml-events";
declare namespace local = "local";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace foaf = "http://xmlns.com/foaf/0.1/";
declare namespace geo = "http://www.w3.org/2003/01/geo/wgs84_pos#";
declare namespace lawd="http://lawd.info/ontology/";
declare namespace pleiades="https://pleiades.stoa.org/places/vocab#";
declare namespace spatial="http://geovocab.org/spatial#";


let $project := request:get-parameter("project", "")
let $placeNumber := count($spatiumStructor:project-place-collection//pleiades:Place)
let $placeId := request:get-parameter("resource", "")
let $placeUriShort := $spatiumStructor:uriBase || "/places/" || $placeId
let $placeUri := $spatiumStructor:uriBase || "/places/" || $placeId || "#this"

let $place := $spatiumStructor:project-place-collection//spatial:Feature[@rdf:about= $placeUri]

let $appGeneralParameters := doc("/db/apps/" || $config:project || "/data/app-general-parameters.xml")

let $moveToPlaceScript := "$(document).ready( function () {{
                var markerCoordinates = markerMap['" || $placeUriShort || "'].getLatLng();
                console.log('lat: ' + markerCoordinates.lat);
                displayMap.flyTo([markerCoordinates.lat, markerCoordinates.lng], 12);
               
                    }});"
let $updateTitleWindow :=if($placeId = "new") then  '$(document).ready( function () {{
                document.title = "' || $project || " - New place" || '";
                    }});'
                        else if ($placeId != "") then '$(document).ready( function () {{
                        var placeName = getCurrentPlaceName();
                        document.title = "Place ' || $placeId || ' - " + placeName;
                    }});' 
                    else '$(document).ready( function () {{
                document.title = "' || $project || " Places" || '";
                    }});'
return

 <div data-template="templates:surround" data-template-with="templates/page.html" data-template-at="content">
    <!--<link rel="stylesheet" href="$ausohnum-lib/resources/scripts/jquery/summernote/summernote.min.css" />
    <script src="$ausohnum-lib/resources/scripts/jquery/summernote/summernote.min.js"></script>
    -->
    <script src="$ausohnum-lib/resources/scripts/jquery/trumbowyg/trumbowyg.js"/>
    <script src="$ausohnum-lib/resources/scripts/jquery/trumbowyg/plugins/teitags/trumbowyg.teitags.js"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.27.3/ui/trumbowyg.min.css"/>
 <!--Script for fancytree-->
        <!-- Include Fancytree skin and library -->
        <link href="$ausohnum-lib/resources/scripts/jquery/fancytree/skin-bootstrap/ui.fancytree.css" rel="stylesheet" type="text/css"/>
        <script src="$ausohnum-lib/resources/scripts/jquery/fancytree/jquery.fancytree-all.min.js" type="text/javascript"/>
        <script src="$ausohnum-lib/resources/scripts/jquery/fancytree/jquery.fancytree.filter.js" type="text/javascript"/>
        <script src="$ausohnum-lib/resources/scripts/jquery/fancytree/jquery.fancytree.glyph.js" type="text/javascript"/>
        <script src="$ausohnum-lib/resources/scripts/jquery/fancytree/jquery.fancytree.wide.js" type="text/javascript"/>
 

 <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" integrity="sha512-07I2e+7D8p6he1SIM+1twR5TIrhUQn9+I6yjqD53JQjFiMf8EtC93ty0/5vJTZGF8aAocvHYNEDJajGdNx1IsQ==" crossorigin="" />

<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet-src.js" integrity="sha512-WXoSHqw/t26DszhdMhOXOkI7qCiv5QWXhH9R7CgvgZMHz1ImlkVQ3uNsiQKu5wwbbxtPzFXd1hK4tzno2VqhpA==" crossorigin=""></script>

  <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet-providers.js"></script>
   <link rel="stylesheet" href="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet-search/src/leaflet-search.css"/>

<!--Markercluster -->
<!-- 
<link rel="stylesheet" href="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.markercluster/MarkerCluster.css"/>
<link rel="stylesheet" href="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.markercluster/MarkerCluster.Default.css"/>
--> 

  <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet-search/src/leaflet-search.js"></script>
  <!--  Leaflet Draw  -->

 <link rel="stylesheet" href="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/leaflet.draw.css"/>
<!--Markercluster -->
<link rel="stylesheet" href="https://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.css" />
<link rel="stylesheet" href="https://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.Default.css" />
<script src="https://leaflet.github.io/Leaflet.markercluster/dist/leaflet.markercluster-src.js"></script>

<!--
<link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css"/>
<link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css"/>
 <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.markercluster/leaflet.markercluster.js"></script>
-->

 <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/Leaflet.draw.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/Leaflet.Draw.Event.js"></script>

    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/Toolbar.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/Tooltip.js"></script>

    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/ext/GeometryUtil.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/ext/LatLngUtil.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/ext/LineUtil.Intersect.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/ext/Polygon.Intersect.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/ext/Polyline.Intersect.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/ext/TouchEvents.js"></script>

    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/draw/DrawToolbar.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/draw/handler/Draw.Feature.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/draw/handler/Draw.SimpleShape.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/draw/handler/Draw.Polyline.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/draw/handler/Draw.Marker.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/draw/handler/Draw.Circle.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/draw/handler/Draw.CircleMarker.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/draw/handler/Draw.Polygon.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/draw/handler/Draw.Rectangle.js"></script>


    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/edit/EditToolbar.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/edit/handler/EditToolbar.Edit.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/edit/handler/EditToolbar.Delete.js"></script>

    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/Control.Draw.js"></script>

    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/edit/handler/Edit.Poly.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/edit/handler/Edit.SimpleShape.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/edit/handler/Edit.Rectangle.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/edit/handler/Edit.Marker.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/edit/handler/Edit.CircleMarker.js"></script>
    <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.draw/edit/handler/Edit.Circle.js"></script>
  <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.featuregroup.subgroup.js"></script>

 
 <script type="text/javascript" src="$ausohnum-lib/resources/scripts/spatiumStructor/L.Control.Geonames.min.js"/>
        <link href="$ausohnum-lib/resources/css/skosThesau.css" rel="stylesheet" type="text/css"/>
        <link href="$ausohnum-lib/resources/css/spatiumStructor.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/resources/scripts/spatiumStructorMarkersOptions.js"/>
        <script type="text/javascript" src="/resources/scripts/spatiumStructor.js"/>
        <script type="text/javascript" src="$ausohnum-lib/resources/scripts/spatiumStructor/spatiumStructorFunctions.js"/>
        <script type="text/javascript" src="/resources/scripts/archaeoTree.js"/>
        <link href="/resources/css/archaeoEditor.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript">{ $updateTitleWindow }
                
        </script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.11.0/underscore-min.js"/>

        <div id="spatiumStructor" class="">
            <div class="hidden" id="placeEditorType">archaeo</div>
           <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2" style="font-size: smaller;">
                                
                                <ul class="nav nav-pills" id="pills-places" role="tablist">
                                         <li class="nav-item active">
                                           <a class="nav-link" id="pills-archaeoTree" data-toggle="pill" href="#nav-archaeoTree" role="tab" aria-controls="pills-archaeoTree" aria-selected="false">{ ausohnumCommons:label("archaeo-treeTab", "Tree") }</a>
                                         </li>
                                         
                                         <li class="nav-item">
                                           <a class="nav-link" id="pills-fullList" data-toggle="pill" href="#nav-fullList" role="tab" aria-controls="pills-fullList" aria-selected="false">{ ausohnumCommons:label("archaeo-allPlacesTab", "All places") }</a>
                                         </li>
                                </ul>
                                <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane fade in active" id="nav-archaeoTree" role="tabpanel" aria-labelledby="nav-archaeoTree">        
                                                <input name="searchTree" id="searchTree" placeholder="Filter places" title="Filter places" autocomplete="off"/>
                                                <button id="btnResetSearch" class="btn btn-default" title="Clear filter">
                                                        <i class="glyphicon glyphicon-remove-sign"/>
                                                </button>
                                                <div id="collection-tree" data-type="json"/>
                                         </div>
                                         <div class="tab-pane fade in" id="nav-fullList" role="tabpanel" aria-labelledby="nav-fullList">        
                                                <button class="btn btn-sm btn-primary" onclick="openNewPlaceForm()">Create a new place</button>
                                                { spatiumStructor:listPlacesAsTable() }
                                          </div>
                                  
                                </div>
                          </div>
                <div class="col-xs5 col-sm-5 col-md-5">
                        <div id="editLocationButton" class="btn btn-primary editbutton hidden"
                             appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                                       editConceptIcon"></i>
                        </div>
                        <div id="placeEditor">{
                                if(($placeId ="") or ($placeId = "root")) then (
                                    <div>
                                      <h1 class="display-4">Places Manager</h1>
                                      <p>Total of places: {$placeNumber}</p>
                                      <p><strong>Display a place record</strong> by selecting in the left-hand menu or by browsing the map.</p>
                                      <p><strong>Create a place</strong> by selecting "Create a new place" at the bottom of the left-hand menu.</p>
                                      <p>
                                      You need to update the <a href="/places/update-gazetteer/">place gazetteer</a></p>
                                      </div>
                                 )
                                else if($placeId ="new") then
                                    placeRecordGenerator:newPlace()
                                else placeRecordGenerator:archaeoManager($placeUriShort, $project)}
                       </div>
                </div>
                
                <div class="col-xs-5 col-sm-5 col-md-5">
                        <div id="placeManagerMap" ></div>
                        <div id="positionInfo"/>
                        <div id="savedPositionInfo">{ ausohnumCommons:label("archaeo-mapGetGeoCoord", "Click to store current position") }: </div>

                    { spatiumStructor:searchPleiadesModal() }
                    { spatiumStructor:searchProjectPlacesModal() }
                    { 
                     spatiumStructor:addResourceDialog("seeFurther") 
                    }
                    { spatiumStructor:addResourceDialog("illustration") }
                    { spatiumStructor:newSubPlaceModal() }
                    { spatiumStructor:addNewDocModal() }
                    
                    </div>
                                </div><!--End of row-->
                    <!--    <div id="mapid"></div>-->
      </div>
<script>
  $("#datanavbar-places").addClass("active");
</script>


 </div>
