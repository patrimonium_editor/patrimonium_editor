(:~
: AusoHNum Library - spatial data manager module
: This module contains the main functions of the spatial data manager module.
: @author Vincent Razanajao
:)

xquery version "3.1";

import module namespace functx="http://www.functx.com";

declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace foaf="http://xmlns.com/foaf/0.1/";
declare namespace geo = "http://www.w3.org/2003/01/geo/wgs84_pos#";
declare namespace json="http://www.json.org";
declare namespace local = "local";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace pleiades="https://pleiades.stoa.org/places/vocab#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare namespace spatial="http://geovocab.org/spatial#";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";


(: Switch to JSON serialization :)
declare option output:method "json";
declare option output:media-type "text/application";

declare variable $project :=request:get-parameter('project', "patrimonium_editor");
declare variable $lang :=request:get-parameter('lang', ());

declare variable $data-collection-path := "/db/apps/" || $project || "Data";
declare variable $data-collection := collection($data-collection-path);
declare variable $place-collection-path := $data-collection-path || "/places/" || $project ;
declare variable $place-collection :=collection($place-collection-path);

declare variable $places-collection := collection("/db/apps/" || $project || "Data/places/" || $project );

declare variable $doc-collection-path := $data-collection-path || "/documents";
declare variable $rootDoc := doc($place-collection-path || "/_root.rdf");

declare function local:buildArchaeoTree($rootNodes, $lang){
                let $collation :=  '?lang=' || lower-case($lang) || "-" || $lang

                for $child in $rootNodes
                let $placeConcept := $child//pleiades:Place
                let $nts := $child//spatial:Pi
                let $title := if($child//pleiades:Place[1]//dcterms:title[@xml:lang=$lang]) then
                            functx:capitalize-first($child//pleiades:Place[1]//dcterms:title[@xml:lang=$lang]/text())
                            else if($child//pleiades:Place[1]//dcterms:title[1]) then
                            functx:capitalize-first($child//pleiades:Place[1]//dcterms:title[1]/text())
                          else ("No label for " || data($child/@rdf:about))
                let $order := data($child/@type)
                order by
                      lower-case($title) collation "?lang=en"

              return

              <children json:array="true" status="{data($child//@status)}" type="collectionItem">
                 <title>{ $title }</title>

                 <uri>{ data($child/@rdf:about) }</uri>
                 <key>{ substring-before(substring-after(data($child/@rdf:about), "/places/"), "#this") }</key>
                 <data>
                 <id>{ substring-before(substring-after(data($child/@rdf:about), "/places/"), "#this") }</id>
                 <lng>{ if($placeConcept/geo:long/text()!= "") then
                                                (
                                                data($placeConcept/geo:long/text())
                                                )
            
                                                else
                                                (
                                                    if($child//spatial:C[@type='isInVicinityOf'])
                                                        then (
                                                            let $parentId := data($child//spatial:C[@type='isInVicinityOf'][1]/@rdf:resource)
                                                            let $parentWithGeoRef :=
                                                                        $place-collection//spatial:Feature[@rdf:about=$parentId[1] || "#this"][1]
                                                            return 
                                                             
                                                            data($parentWithGeoRef//pleiades:Place[1]/geo:long[1])
                                                            )
                                                      else(
                                                            let $Ps := $child/spatial:P
                                                            let $parentId := data($Ps/@rdf:resource)
            
                                                            let $parentWithGeoRef :=
                                                                    for $p in $Ps
                                                                        let $pId := data($p/@rdf:resource)
                                                                        let $pPlace := $place-collection//pleiades:Place[@rdf:about=$pId]
                                                                        return
                                                                        if($pPlace[not(matches(.//pleiades:hasFeatureType/text(), "province") )]) then
                                                                        $pPlace
                                                                        else 
                                                                        (<pleiades:Place/>)
            
                                                       return
                                                       data($parentWithGeoRef[1]//geo:long)
            
                                                    )
                                                    )
                                                }</lng>
                 <lat>{ if($placeConcept/geo:lat/text() != "") then
                                                (
                                                data($placeConcept/geo:lat/text())
                                                )
            
                                                else
                                                (
                                                    if($child//spatial:C[@type='isInVicinityOf'])
                                                        then (
                                                            let $parentId := data($child/spatial:C[@type='isInVicinityOf'][1]/@rdf:resource)
                                                            let $parentWithGeoRef :=  $place-collection//spatial:Feature[@rdf:about=$parentId[1] || "#this"][1]
                                                            return  data($parentWithGeoRef//pleiades:Place[1]/geo:lat[1])
                                                            )
                                                      else(
                                                            let $Ps := $child/spatial:P
                                                            let $parentId := data($child//spatial:P/@rdf:resource)
            
                                                            let $parentWithGeoRef :=
                                                                    for $p in $Ps
                                                                        let $pId := data($p/@rdf:resource)
                                                                        let $pPlace := $place-collection//pleiades:Place[@rdf:about=$pId]
                                                                        return
                                                                        if($pPlace[not(matches(.//pleiades:hasFeatureType/text(), "province") )]) then
                                                                        $pPlace
                                                                        else (<pleiades:Place/>)
            
                                                       return
                                                       data($parentWithGeoRef[1]//geo:lat)
            
                                                    )
                                                    )
                                                }</lat>
                 <lang>{$lang}</lang>
                </data>
                 <isFolder>true</isFolder>
                 { local:placeNodes($nts, (), data($child/@type), $lang)}
              </children>



 };

 declare function local:placeNodes($nodes, $visited, $renderingOrder, $lang){

            for $childnodes in $nodes except ($visited)
                let $uri := data($childnodes/@rdf:resource) || "#this"
                let $ntConcept :=
                    $place-collection//spatial:Feature[@rdf:about=$uri]
                 let $placeConcept := $ntConcept[1]//pleiades:Place
                let $title := if($placeConcept//dcterms:title[@xml:lang=$lang]) then
                                (functx:capitalize-first($placeConcept//dcterms:title[@xml:lang=$lang]/text())
                                )

                                else if ($placeConcept/dcterms:title[@xml:lang='en']/text()) then
                                (concat(functx:capitalize-first($ntConcept//pleiades:Place/dcterms:title[@xml:lang='en']/text()), ' (en)'))
                                else if  ($placeConcept/dcterms:title[@xml:lang='fr']/text()) then
                                (concat(functx:capitalize-first($placeConcept/dcterms:title[@xml:lang='fr']/text()), ' (fr)'))



                                else ("No name found for " || data($ntConcept/@rdf:about))
                                || " [" || substring-before(substring-after($placeConcept/@rdf:about, "places/"), "#this") || "]"
                                
                let $order := data($placeConcept/@ype)

                order by
                    if ($renderingOrder = "ordered") then reverse($childnodes)
                    else (lower-case(

                            if(exists($placeConcept/dcterms:title[@xml:lang=$lang])) then
                           translate($placeConcept/dcterms:title[@xml:lang=$lang]/text(),'Â, Ê, É','A, E, E')
                            else if(exists($placeConcept/dcterms:title[@xml:lang="en"])) then
                            translate($placeConcept/dcterms:title[@xml:lang="en"]/text(),'Â, Ê, É','A, E, E')
                            else if(exists($placeConcept/dcterms:title[@xml:lang="fr"])) then
                            translate($placeConcept/dcterms:title[@xml:lang="fr"]/text(),'Â, Ê, É','A, E, E')
                            else if(exists($placeConcept/dcterms:title[@xml:lang="de"])) then
                            translate($placeConcept/dcterms:title[@xml:lang="de"]/text(),'Â, Ê, É','A, E, E')
                            else(
                            translate($placeConcept/dcterms:title[@xml:lang="en"]/text(),'Â, Ê, É','A, E, E'))
                    ))
                     (:
                     if($renderingOrder ="ordered") then order by reverse($childnodes)
                     else order by $ntSkosConcept/skos:prefLabel[@xml:lang=$lang]:)

    return

                if ($ntConcept//spatial:Pi)
                  then(
                       <children json:array="true" status="{data($ntConcept//@status)}" type="collectionItem">
                                <title>
                                { $title }</title>
                                <id>{ substring-before(substring-after($uri, "/places/"), "#this") }</id>
                                
                                <!--<key>{ substring-before(substring-after(data($ntConcept/@rdf:about), "/places/"), "#this") }</key>-->
                                <key>{ substring-before(substring-after($uri, "/places/"), "#this") }</key>
                                <data>
                                <id>{ substring-before(substring-after($uri, "/places/"), "#this") }</id>
                                
                                <!--<key>{ substring-before(substring-after(data($ntConcept/@rdf:about), "/places/"), "#this") }</key>-->
                                <key>{ substring-before(substring-after($uri, "/places/"), "#this") }</key>
                                <uri>{$uri}</uri>
                                {""
(:                                substring-before(substring-after(data($ntConcept/@rdf:about), "/places/"), "#this") :)
                                }
                                 
                                 <lng>{ if($placeConcept/geo:long/text()!= "") then
                                                (
                                                data($placeConcept/geo:long/text())
                                                )
            
                                                else
                                                (
                                                    if($placeConcept//spatial:C[@type='isInVicinityOf'])
                                                        then (
                                                            let $parentId := data($placeConcept//spatial:C[@type='isInVicinityOf'][1]/@rdf:resource)
                                                            let $parentWithGeoRef :=
                                                                        $place-collection//spatial:Feature[@rdf:about=$parentId || "#this"][1]
                                                            return 
                                                             
                                                            data($parentWithGeoRef//pleiades:Place[1]//geo:long[1])
                                                            )
                                                      else if($placeConcept/spatial:Pi) then (
                                                            let $Ps := $placeConcept/spatial:Pi
                                                            let $parentId := data($Ps[1]/@rdf:resource)
            
                                                            let $parentWithGeoRef :=
                                                                    for $p in $Ps
                                                                        let $pId := data($p/@rdf:resource)
                                                                        let $pPlace := $place-collection//pleiades:Place[@rdf:about=$pId]
                                                                        return
                                                                        if($pPlace[not(matches(.//pleiades:hasFeatureType/text(), "province") )]) then
                                                                        $pPlace
                                                                        else 
                                                                        (<pleiades:Place/>)
            
            
                                                       return
                                                       data($parentWithGeoRef[1]//geo:long)
            
                                                    )
                                                     
                                                     
                                                     
                                                     
                                                     else(
                                                            let $Ps := $placeConcept/spatial:P
                                                            let $parentId := data($Ps/@rdf:resource)
            
            
                                                            let $parentWithGeoRef :=
                                                                    for $p in $Ps
                                                                        let $pId := data($p/@rdf:resource)
                                                                        let $pPlace := $place-collection//pleiades:Place[@rdf:about=$pId]
                                                                        return
                                                                        if($pPlace[not(matches(.//pleiades:hasFeatureType/text(), "province") )]) then
                                                                        $pPlace
                                                                        else 
                                                                        (<pleiades:Place/>)
            
                                                       return
                                                       data($parentWithGeoRef[1]//geo:long)
            
                                                    )
                                                    
                                                    
                                                    )
                                                }</lng>
                 <lat>{ if($placeConcept/geo:lat/text() != "") then
                                                (
                                                data($placeConcept/geo:lat/text())
                                                )
            
                                                else
                                                (
                                                    if($placeConcept//spatial:C[@type='isInVicinityOf'])
                                                        then (
                                                            let $parentId := data($placeConcept/spatial:C[@type='isInVicinityOf'][1]/@rdf:resource)
                                                            let $parentWithGeoRef :=  $place-collection//spatial:Feature[@rdf:about=$parentId || "#this"][1]
                                                            return  data($parentWithGeoRef//pleiades:Place[1]//geo:lat[1])
                                                            )
                                                            
                                                      else if ($placeConcept/spatial:Pi) then (
                                                            let $Ps := $placeConcept/spatial:Pi
                                                            let $parentId := data($placeConcept//spatial:Pi[1]/@rdf:resource)
            
            
                                                            let $parentWithGeoRef :=
                                                                    for $p in $Ps
                                                                        let $pId := data($p/@rdf:resource)
                                                                        let $pPlace := $place-collection//pleiades:Place[@rdf:about=$pId]
                                                                        return
                                                                        if($pPlace[not(matches(.//pleiades:hasFeatureType/text(), "province") )]) then
                                                                        $pPlace
                                                                        else (<pleiades:Place/>)
            
                                                       return
                                                       data($parentWithGeoRef[1]//geo:lat)
            
                                                    )
                                                    
                                                      else(
                                                            let $Ps := $placeConcept/spatial:P
                                                            let $parentId := data($placeConcept//spatial:P/@rdf:resource)
            
            
                                                            let $parentWithGeoRef :=
                                                                    for $p in $Ps
                                                                        let $pId := data($p/@rdf:resource)
                                                                        let $pPlace := $place-collection//pleiades:Place[@rdf:about=$pId]
                                                                        return
                                                                        if($pPlace[not(matches(.//pleiades:hasFeatureType/text(), "province") )]) then
                                                                        $pPlace
                                                                        else (<pleiades:Place/>)
            
                                                       return
                                                       data($parentWithGeoRef[1]//geo:lat)
            
                                                    )
                                                    )
                                                }</lat>
                                 
                                 
                                 
                                 
                                 
                                </data>
                                <lang>{$lang}</lang>
                                <isFolder>true</isFolder>
                                { local:placeNodes($ntConcept//spatial:Pi, ($visited, $childnodes), data($ntConcept/@type), $lang)
                                        }
                        </children>
                    )
                    else
                    (
                    <children json:array="false" status="{data($ntConcept//@status)}" type="collectionItem">
                        <title>{ $title }{" [" || substring-before(substring-after($uri, "places/"), "#this") || "]"}</title>
                          
                                    <id>{ substring-before(substring-after(data($uri), "/places/"), "#this")}</id>
                                    
                                    <key>{ substring-before(substring-after(data($uri), "/places/"), "#this")}</key>
                                    <data>
                                    <uri>{$uri}</uri>
                                    <lng>{ if($placeConcept/geo:long/text() !="") then
                                                (
                                                data($placeConcept/geo:long/text())
                                                )
            
                                                else
                                                (
                                                    if($ntConcept//spatial:C[@type='isInVicinityOf'])
                                                        then (
                                                        
                                                            let $parentId := data($ntConcept[1]/spatial:C[@type='isInVicinityOf'][1]/@rdf:resource)
                                                            
                                                            let $parentWithGeoRef :=  $place-collection//spatial:Feature[@rdf:about=$parentId[1] || "#this"][1]
                                                            
                                                            return  data($parentWithGeoRef//pleiades:Place[1]/geo:long[1])
                                                            )
                                                      
                                                      else(
                                                            let $Ps := $ntConcept/spatial:P
                                                            let $parentId := data($ntConcept//spatial:P[1]/@rdf:resource)
            
            
                                                            let $parentWithGeoRef :=
                                                                    for $p in $Ps
                                                                        let $pId := data($p/@rdf:resource)
                                                                        let $pPlace := $place-collection//pleiades:Place[@rdf:about=$pId]
                                                                        return
                                                                        if($pPlace[not(matches(.//pleiades:hasFeatureType/text(), "province") )]) then
                                                                        $pPlace
                                                                        else (<pleiades:Place/>)
            
                                                       return
                                                       data($parentWithGeoRef[1]//geo:long)
            
                                                    )
                                                    )
                                                }
                                    </lng>
                                    <lat>{ if($placeConcept/geo:lat/text() != "") then
                                                (
                                                data($placeConcept/geo:lat/text())
                                                )
            
                                                else
                                                (
                                                    if($ntConcept//spatial:C[@type='isInVicinityOf'])
                                                        then (
                                                            let $parentId := data($ntConcept[1]/spatial:C[@type='isInVicinityOf'][1]/@rdf:resource)
                                                            let $parentWithGeoRef :=  $place-collection//spatial:Feature[@rdf:about=$parentId[1] || "#this"][1]
                                                            return  data($parentWithGeoRef//pleiades:Place[1]/geo:lat[1])
                                                            )
                                                      else(
                                                            let $Ps := $ntConcept/spatial:P
                                                            let $parentId := data($ntConcept//spatial:P/@rdf:resource)
            
                                                            let $parentWithGeoRef :=
                                                                    for $p in $Ps
                                                                        let $pId := data($p/@rdf:resource)
                                                                        let $pPlace := $place-collection//pleiades:Place[@rdf:about=$pId]
                                                                        return
                                                                        if($pPlace[not(matches(.//pleiades:hasFeatureType/text(), "province") )]) then
                                                                        $pPlace
                                                                        else (<pleiades:Place/>)
            
                                                       return
                                                       data($parentWithGeoRef[1]//geo:lat)
            
                                                    )
                                                    )
                                                }
            
            
            
            
                                    </lat>
                        <lang>{$lang}</lang>
                        </data>
                    </children>
                    )
         (:{ if($ntConcept//pleiades:Place/geo:lat) then
                                    data($ntConcept//pleiades:Place/geo:lat/text())
                                    else
                                    (
                                    let $parentId := data($ntConcept[1]//spatial:P[1]/@rdf:resource)
                                    let $parentWithGeoRef :=  $spatiumStructor:place-collection//spatial:Feature[@rdf:about=$parentId || "#this"]
                                    return data($parentWithGeoRef//pleiades:Place/geo:lat))}
                        :)


};

 let $rootPlaces := $rootDoc//spatial:Pi
 let $rootNodesFromRootFile :=
        for $place in $rootPlaces
        let $placeUri := $place/@rdf:resource || "#this"
        return
        $places-collection//spatial:Feature[@rdf:about=$placeUri]
 
 let $rootNodes :=
         $places-collection//spatial:Feature
 let $rootNodesByRegions :=
         $places-collection//spatial:Feature[foaf:primaryTopicOf/pleiades:Place//pleiades:hasFeatureType[@rdf:resource='https://ausohnum.huma-num.fr/concept/c21863']]

(: for $child in $children:)
 return

        
        <children xmlns:json="http://www.json.org" json:array="true">
         <title>{ switch($lang)
                                   case "fr" return "Explorer les Lieux"
                                    default return "Browse Places" }</title>
         <id>{data($rootDoc/@rdf:about)}</id>
         <key>{data($rootDoc/@rdf:about)}</key>
         <lng>{ data($rootDoc//pleiades:Place/geo:long/text()) }</lng>
         <lat>{ data($rootDoc//pleiades:Place/geo:lat/text()) }</lat>
          <isFolder>true</isFolder>
         <orderedCollection json:literal="true">true</orderedCollection>
         <lang></lang>
                        
                       <children xmlns:json="http://www.json.org" json:array="true">
                         <title>{ switch($lang)
                                   case "fr" return "Par régions"
                                    default return "By regions" }</title>
                         <!--
                         <id>{data($rootDoc/@rdf:about)}</id>
                         <key>{data($rootDoc/@rdf:about)}</key>
                         <lng>{ data($rootDoc//pleiades:Place/geo:long/text()) }</lng>
                         <lat>{ data($rootDoc//pleiades:Place/geo:lat/text()) }</lat>
                         -->
                          <isFolder>true</isFolder>
                         <orderedCollection json:literal="true">true</orderedCollection>
                         <lang></lang>
                                    { local:buildArchaeoTree($rootNodesByRegions, $lang)}
                       </children>
                       <children xmlns:json="http://www.json.org" json:array="true">
                         <title>{ switch($lang)
                                   case "fr" return "Par ordre alphabétique"
                                    default return "By Alphabetical Order" }</title>
                         <!--
                         <id>{data($rootDoc/@rdf:about)}</id>
                         <key>{data($rootDoc/@rdf:about)}</key>
                         <lng>{ data($rootDoc//pleiades:Place/geo:long/text()) }</lng>
                         <lat>{ data($rootDoc//pleiades:Place/geo:lat/text()) }</lat>
                         -->
                          <isFolder>true</isFolder>
                         <orderedCollection json:literal="true">true</orderedCollection>
                         <lang></lang>
                                    { local:buildArchaeoTree($rootNodes, $lang)}
                       </children>
                       <children xmlns:json="http://www.json.org" json:array="true">
                         <title>{ switch($lang)
                                    case "en" return "Create a new place"
                                    case "fr" return "Créer un nouveau lieu"
                                    default return "Create a new place" }</title>
                         <id>new</id>
                         <key>new</key>
                         <lng></lng>
                         <lat></lat>
                          <isFolder>false</isFolder>
                         <lang></lang>
                         
                       </children>

    </children>