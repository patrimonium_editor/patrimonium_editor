xquery version "3.1";

module namespace placeRecordGenerator = "http://ausonius.huma-num.fr/placeRecordGenerator";

import module namespace config="https://ausohnum.huma-num.fr/apps/patrimonium_editor/config" at "../config.xqm";
import module namespace console="http://exist-db.org/xquery/console";
import module namespace functx="http://www.functx.com";
import module namespace http="http://expath.org/ns/http-client";

import module namespace ausohnumCommons="http://ausonius.huma-num.fr/commons"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/commons/commonsApp.xql";
import module namespace spatiumStructor="http://ausonius.huma-num.fr/spatiumStructor"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/spatiumStructor/spatiumStructor.xql";
import module namespace skosThesau="https://ausohnum.huma-num.fr/skosThesau/" at "xmldb:exist:///db/apps/ausohnum-library/modules/skosThesau/skosThesauApp.xql";


declare namespace apc = "https://ausohnum.huma-num.fr/apps/patrimonium_editor/onto#";
declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace ev = "http://www.w3.org/2001/xml-events";
declare namespace local = "local";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace foaf = "http://xmlns.com/foaf/0.1/";
declare namespace geo = "http://www.w3.org/2003/01/geo/wgs84_pos#";
declare namespace lawd="http://lawd.info/ontology/";
declare namespace pleiades="https://pleiades.stoa.org/places/vocab#";
declare namespace spatial="http://geovocab.org/spatial#";

(: declare variable $placeRecordGenerator:appGeneralParameters := doc("/db/apps/" || $config:project || "/data/app-general-parameters.xml"); :)
declare variable $placeRecordGenerator:lang := request:get-parameter("lang", "");

declare function placeRecordGenerator:recordForDisplay($placeUriShort as xs:string){
let $placeNumber := count($spatiumStructor:project-place-collection//pleiades:Place)
let $placeId := functx:substring-after-last($placeUriShort , "/")

let $placeUriLong := $placeUriShort || "#this"

let $log := console:log("uri: " )
let $currentUser := sm:id()//sm:real/sm:username/string()
let $groups := string-join(sm:get-user-groups($currentUser), ' ')
let $userRights :=
        if (matches($groups, ('sandbox'))) then "sandbox"
        else if(matches($groups, ('patrimonium'))) then "editor"
        else ("guest")
let $gazetteerRecord := $spatiumStructor:placeGazetteer//features[.//uri = normalize-space($placeUriShort)]
let $coordinates := $gazetteerRecord//coordList/coordinates[1]/text()

(:     let $placeRdf := util:eval('collection("' || $spatiumStructor:project-place-collection-path || '")//spatial:Feature[@rdf:about="' || $placeUriLong || '"][1]'):)
   return
   ((<http:response status="200"> 
                    <http:header name="Cache-Control" value="no-cache"/> 
                 </http:response> 
     ),
            if($spatiumStructor:place-collection//spatial:Feature[@rdf:about= $placeUriLong]) then (
               
               let $placeRdf :=  $spatiumStructor:project-place-collection//spatial:Feature[@rdf:about= $placeUriLong]
                  
                  let $isMadeOfUris := if($placeRdf//spatial:Pi) then <uris>{ $placeRdf//spatial:Pi }</uris> else <none/>
                  let $isPartOfUris := if($placeRdf//spatial:P) then <uris> { $placeRdf//spatial:P }</uris> else <none/>
                  let $isInVicinityOfUris := if($placeRdf//spatial:C[@type='isInVicinityOf']) then <uris> { $placeRdf//spatial:C[@type='isInVicinityOf'] }</uris> else <none/>
                  let $hasInItsVicinityUris:= if($placeRdf//spatial:C[@type='hasInItsVicinity']) then <uris> { $placeRdf//spatial:C[@type='hasInItsVicinity'] }</uris> else <none/>
                  let $isAdjacentToUris:= if($placeRdf//spatial:C[@type='isAdjacentTo']) then <uris> { $placeRdf//spatial:C[@type='isAdjacentTo'] }</uris> else <none/>
                  let $placeName := $placeRdf//dcterms:title/text()
                  
                 
                let $hasSize :=if($placeRdf//apc:hasSize) then 
                 <div class="xmlElementGroup">
                                      <div>
                                        {ausohnumCommons:elementLabel("Size", "simple", ())}{ausohnumCommons:displayElement('hasSizeValue', (), 'inLinePlainText', (), ())/string()}
                                        {ausohnumCommons:displayElement('hasSizeType', (), 'inLinePlainText', (), ())}
                                        {if($placeRdf//apc:hasSize/text() != "") then " (" || 
                                        $placeRdf//apc:hasSize/text()
                                        || ")"
                                        else ()
                                        }
                                        </div>
                                        </div>
                        
                    else()
                let $hasYield :=if($placeRdf//apc:hasYield) then 
                 <div class="xmlElementGroup">
                                      {ausohnumCommons:elementLabel("Yield", "simple", ())}{ausohnumCommons:displayElement('hasYieldValue', (), 'inLinePlainText', (), ())/string()}{ausohnumCommons:displayElement('hasYieldType', (), 'inLinePlainText', (), ())/string()}
                                      { if($placeRdf//apc:hasYield/text() != "") then " (" || $placeRdf//apc:hasYield/text() || ")"
                                      else ()
                                      }
                                      <div>
                                     
                                        
                                        
                                        </div>
                                        </div>
                        
                    else()
                
                return
                
                <div id="placeDetails">
                
                <div class="">
                { spatiumStructor:variables($placeUriShort, $spatiumStructor:project) }
                <div id="currentPlaceCoordinates" class="hidden">{ $coordinates }</div>
            <!--    <script type="text/javascript" src="$ausohnum-lib/resources/scripts/spatiumStructor/spatiumStructor.js"/>
                
                  
                  <link href="$ausohnum-lib/resources/css/spatiumStructor.css" rel="stylesheet" type="text/css"/>
                  -->
             <!--
             <link rel="stylesheet" href="$ausohnum-lib/resources/css/teiEditor.css"/>
             -->
             
                <h3 id="resourceTitle"><span>{ $placeId }.</span>{ ausohnumCommons:displayElement('title', (), 'inLinePlainText', (), ()) }</h3>
                <span id="currentPlaceUri" class="hidden">{ $placeUriShort }</span>
                <h4>URI { $placeUriShort } {ausohnumCommons:copyValueToClipboardButton("uri", 1, $placeUriShort)}</h4>
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                         <li class="nav-item active">
                                           <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#nav-metadata" role="tab" aria-controls="pills-home" aria-selected="false">Place details</a>
                                         </li>
                                         
                                         <li class="nav-item">
                                           <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#nav-xmlfile" role="tab" aria-controls="pills-profile" aria-selected="false">XML</a>
                                         </li>
                                     </ul>
                                     <div class="tab-content" id="nav-tabContent">
                                         <div class="tab-pane fade in active" id="nav-metadata" role="tabpanel" aria-labelledby="nav-metadata-tab">
                                                {ausohnumCommons:displayElement('hasFeatureTypeMain', (), 'simple', (), ())                                                 } 
                                                
                                                
                                                { ausohnumCommons:displayElement('altLabel', (), 'simple', (), ()) }
                                                { ausohnumCommons:displayElement('exactMatch', 'External resource(s)', 'simple', (), ()) }
                                                
                                                { ausohnumCommons:displayElement('productionType', (), 'simple', (), ()) }
                                                { $hasSize }
                                                { $hasYield }
                                                { ""
                                                (:ausohnumCommons:temporalRangeAttestations($placeUriShort) :)}
                                                
                                                { spatiumStructor:representativeCoordinates($placeUriShort) }
                                                <div id="temporalScaleListHolder" style="margin-bottom: 1em">
                                                <img id="f-load-indicator" class="" src="/resources/images/ajax-loader.gif" style="margin-right: 1em;"/>... retrieving temporal range of attestations in documents...
                                                </div>
                                                { ausohnumCommons:biblioAndResourcesList($placeRdf, "seeFurther") }
                                                <div id="relatedPlacesListHolder" style="margin-bottom: 1em">
                                                <img id="f-load-indicator" class="" src="/resources/images/ajax-loader.gif" style="margin-right: 1em;"/>... retrieving related places...
                                                </div>
                                                
                                                {""
                                                (:ausohnumCommons:relatedPlacesToPlace($placeUriLong, "isPartOf")
                                                { ausohnumCommons:relatedPlacesToPlace($placeUriLong, "isMadeOf")
                                                { ausohnumCommons:relatedPlacesToPlace($placeUriLong, "isInVicinityOf")
                                                { ausohnumCommons:relatedPlacesToPlace($placeUriLong, "hasInItsVicinity")
                                                { ausohnumCommons:relatedPlacesToPlace($placeUriLong, "isAdjacentTo")
                                                { ausohnumCommons:relatedDocuments($placeUriShort, "place") :)}                                                
                                                <div id="relatedDocumentsListHolder" style="margin-bottom: 1em">
                                                <img id="f-load-indicator" class="" src="/resources/images/ajax-loader.gif" style="margin-right: 1em;"/>... retrieving related documents...
                                                </div>
                                                
                                                <div id="relatedPeopleListHolder" style="margin-bottom: 1em">
                                                <img id="f-load-indicator" class="" src="/resources/images/ajax-loader.gif" style="margin-right: 1em;"/>... retrieving related people...
                                                </div>
                                                {""
(:                                                ausohnumCommons:relatedPeople($placeUriShort, "place") :)
                                                }
                                                
                                                { ausohnumCommons:displayElement('generalCommentary', (), 'simple', (), ()) }
                                                { ausohnumCommons:displayElement('keywords', (), 'simple', (), ()) }
                                           
                                        </div>
                                     <div class="tab-pane fade in" id="nav-xmlfile" role="tabpanel" aria-labelledby="nav-text-tab">
                                          
                                          {
                                          
                                          ausohnumCommons:displayXMLFile($placeUriShort) }
                                            
                                                              
                                      </div>
                                      </div>
                                      </div>
                                      <script>//console.log("Editor required");
                                      
             var editor4File = ace.edit("xmlFile");
                   editor4File.session.setMode("ace/mode/xml");
                   editor4File.setOptions({{
                        readOnly: true,
                         minLines: 40,
                         maxLines: Infinity}});
                         
              function getXmlEditorContent(){{
                     var xmlFileEditor = ace.edit("xmlFile");
                     return xmlFileEditor.getValue();
                      
              }};           
              $(document).ready(function() {{
                $("#temporalScaleListHolder").load("/places/get-temporal-scale/" + {$placeId});
              $("#relatedPlacesListHolder").load("/places/get-related-places/" + {$placeId});
              $("#relatedDocumentsListHolder").load("/places/get-related-documents/" + {$placeId});
              $("#relatedPeopleListHolder").load("/places/get-related-people/" + { $placeId });
              
              }});                                        
             </script>
                </div>
                )
                else (
                <div class="jumbotron jumbotron-fluid">
                        <div class="container">
                          <h1 class="display-4">Error!</h1>
                          <p class="lead">There is no place with URI { $placeUriLong }</p>
                        </div>
                      </div>
                )
 )
 };
 
 declare function placeRecordGenerator:recordForEditing($placeUriShort as xs:string){
let $placeNumber := count($spatiumStructor:project-place-collection//pleiades:Place)
let $placeId := functx:substring-before-last($placeUriShort , "/")

let $placeUriLong := $placeUriShort || "#this"

let $log := console:log("uri: " )
let $currentUser := sm:id()//sm:real/sm:username/string()
let $groups := string-join(sm:get-user-groups($currentUser), ' ')
let $userRights :=
        if (matches($groups, ('sandbox'))) then "sandbox"
        else if(matches($groups, ('patrimonium'))) then "editor"
        else ("guest")

let $placeUriLong := xmldb:decode-uri($placeUriLong)
(:     let $placeRdf := util:eval('collection("' || $spatiumStructor:project-place-collection-path || '")//spatial:Feature[@rdf:about="' || $placeUriLong || '"][1]'):)
   
   return
   ((<http:response status="200"> 
                    <http:header name="Cache-Control" value="no-cache"/> 
                    <http:header name="TESTUM" value="no-cache"/>
                </http:response> 
     ),
            if($spatiumStructor:place-collection//spatial:Feature[@rdf:about= $placeUriLong]) then (
               
               let $placeRdf :=  $spatiumStructor:project-place-collection//spatial:Feature[@rdf:about= $placeUriLong]
                  
                  let $isMadeOfUris := if($placeRdf//spatial:Pi) then <uris>{ $placeRdf//spatial:Pi }</uris> else <none/>
                  let $isPartOfUris := if($placeRdf//spatial:P) then <uris> { $placeRdf//spatial:P }</uris> else <none/>
                  let $isInVicinityOfUris := if($placeRdf//spatial:C[@type='isInVicinityOf']) then <uris> { $placeRdf//spatial:C[@type='isInVicinityOf'] }</uris> else <none/>
                  let $hasInItsVicinityUris:= if($placeRdf//spatial:C[@type='hasInItsVicinity']) then <uris> { $placeRdf//spatial:C[@type='hasInItsVicinity'] }</uris> else <none/>
                  let $isAdjacentToUris:= if($placeRdf//spatial:C[@type='isAdjacentTo']) then <uris> { $placeRdf//spatial:C[@type='isAdjacentTo'] }</uris> else <none/>
                  let $placeName := $placeRdf//dcterms:title/text()
                  
                 let $relatedDocs := spatiumStructor:relatedDocuments($placeUriShort)
                 let $docs := <div class="xmlElementGroup">
                                      <span class="subSectionTitle">List of documents linked to this place</span>
                                      {
                                            if( count($relatedDocs)> 10) then (<div>These are the 10 first documents on {count($relatedDocs)} in total</div>) else()}
                                      <div id="listOfDocuments">
                                      {if($relatedDocs) then
                                      (
                                      <ul class="listNoBullet">{
                                        for $doc in $relatedDocs[position() < 11] 
             (:                           $spatiumStructor:doc-collection//tei:TEI[descendant-or-self::tei:placeName[@ref=$placeUriShort]]:)
                                         (:$spatiumStructor:doc-collection//tei:TEI[tei:listPlace//tei:place//tei:placeName[@ref=$placeUriShort]]:)
                                                let $docId := data($doc/@xml:id)
                                                let $title := $doc//tei:titleStmt/tei:title[1]/text()
                                                let $docUri := if($doc//tei:fileDesc/tei:publicationStmt/tei:idno[@type="uri"]) then $doc//tei:fileDesc/tei:publicationStmt/tei:idno[@type="uri"]
                                                                         else $spatiumStructor:uriBase || "/documents/" || $docId
                                                let $placeType := (
                                                                             let $placeNodes := 
                                                                             $doc//tei:listPlace//tei:place//tei:placeName[contains(./@ref, $placeUriShort)]
                                                                             for $placeNode in $placeNodes
                                                                                 return
                                                                                     if ($placeNode/@ana)
                                                                                     then data($placeNode/@ana)
                                                                                     else if (not($placeNode/@ana)) then ($placeNode/parent::node()/name())
                                                                                     else ()
                                                                                     )
                                                          return
                                                             <li>
                                                             <span class="glyphicon glyphicon-file"/><a href="{ $docUri }" title="Open document { $docUri }" target="_self">{ $title }</a>
                                                             
                                                                             <span>[{""
(:                                                                             $placeType :)
                                                                             }]</span><a href="{ $docUri }" title="Open document { $docUri } in a new window" target="_blank">
                                                                             <i class="glyphicon glyphicon-new-window"/></a>
                                                             </li>
                                         
                                         }</ul>)
                                         
                                         else (<em>None</em>)}
                                         </div>
                                         
                                  </div>
                
                
                return
                
                <div id="placeDetails">
            <!--    <script type="text/javascript" src="$ausohnum-lib/resources/scripts/spatiumStructor/spatiumStructor.js"/>
                
                  
                  <link href="$ausohnum-lib/resources/css/spatiumStructor.css" rel="stylesheet" type="text/css"/>
                  -->
             <!--
             <link rel="stylesheet" href="$ausohnum-lib/resources/css/teiEditor.css"/>
             -->
             
                <h3 id="resourceTitle">{ $placeName }</h3>
                <span id="currentPlaceUri" class="hidden">{ $placeUriShort }</span>
                <h4>URI { $placeUriShort }</h4>
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                         <li class="nav-item active">
                                           <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#nav-metadata" role="tab" aria-controls="pills-home" aria-selected="false">Place details</a>
                                         </li>
                                         
                                         <li class="nav-item">
                                           <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#nav-xmlfile" role="tab" aria-controls="pills-profile" aria-selected="false">XML</a>
                                         </li>
                                     </ul>
                                     <div class="tab-content" id="nav-tabContent">
                                         <div class="tab-pane fade in active" id="nav-metadata" role="tabpanel" aria-labelledby="nav-metadata-tab">
                                     
                                                { spatiumStructor:displayElement('title', $placeUriLong, (), ()) }
                                                 { spatiumStructor:displayElement('altLabel', $placeUriLong, (), ()) }
                                                {spatiumStructor:displayElement('exactMatch', $placeUriLong, (), ())}
                                                { spatiumStructor:displayElement('hasFeatureTypeMain', $placeUriLong, (), ()) }
                                                { spatiumStructor:displayElement('productionType', $placeUriLong, (), ()) }
                                                
                                                {spatiumStructor:placeLocation($placeUriLong)}
                                                 { spatiumStructor:isPartOf($placeUriLong, $isPartOfUris)}
                                                { spatiumStructor:placeConnectedWith($placeUriLong, "isInVicinityOf", $isInVicinityOfUris)}
                                                { spatiumStructor:placeConnectedWith($placeUriLong, "hasInItsVicinity", $hasInItsVicinityUris)}
                                                { spatiumStructor:placeConnectedWith($placeUriLong, "isAdjacentTo", $isAdjacentToUris)}
                                                { spatiumStructor:isMadeOf($placeUriLong, $isMadeOfUris)}
                                                
                                                  


                                           { $docs }
                                           {spatiumStructor:relatedPeople($placeUriShort)}
                                           {
                                   spatiumStructor:resourcesManager('seeFurther', $placeUriShort)
                                   }
                                          { spatiumStructor:displayElement('generalCommentary', $placeUriLong, (), ()) }
                                           { spatiumStructor:displayElement('privateCommentary', $placeUriLong, (), ()) }
                                           
                                        </div>
                                     <div class="tab-pane fade in" id="nav-xmlfile" role="tabpanel" aria-labelledby="nav-text-tab">
                                          { spatiumStructor:xmlFileEditorWithUri($placeUriLong) }
                                                              
                                      </div>
                                      </div>
                                      <script>console.log("Editor required");
             var editor4File = ace.edit("xml-editor-file");
                   editor4File.session.setMode("ace/mode/xml");
                   editor4File.setOptions({{
                         minLines: 40,
                         maxLines: Infinity}});
                         
              function getXmlEditorContent(){{
                     var xmlFileEditor = ace.edit("xml-editor-file");
                     return xmlFileEditor.getValue();
                      
              }};           
             </script>
                </div>
                )
                else (
                <div class="jumbotron jumbotron-fluid">
                        <div class="container">
                          <h3 class="display-4">Error!</h3>
                          <p class="lead">There is no Place with URI { $placeUriShort }</p>
                        </div>
                      </div>
                )
 )
 };

 declare function placeRecordGenerator:archaeoManager($uri as xs:string, $project as xs:string?){
(:    Function to display place details
:)
let $project-place-collection-path := "/db/apps/" || $project || "Data/places/" || $spatiumStructor:project 
let $currentUser := sm:id()//sm:real/sm:username/string()
let $groups := string-join(sm:get-user-groups($currentUser), ' ')
let $userRights :=
        if (matches($groups, ('sandbox'))) then "sandbox"
        
        else if(matches($groups, ($spatiumStructor:project))) then "editor"
        
        
        else ("guest")
     
     let $uri := if(contains($uri, "http")) then $uri else $spatiumStructor:uriBase || "/places/" || $uri
     let $uriShort := if (matches($uri, '#this')) then substring-before($uri, '#this') else $uri
     let $decodedUri := xmldb:decode-uri(if (matches($uri, '#this')) then $uri else $uri || "#this")
     let $placeRdf := collection($project-place-collection-path)//rdf:RDF[spatial:Feature[@rdf:about=$decodedUri]]
(:     let $placeRdf := util:eval('collection("' || $project-place-collection-path || '/")//rdf:RDF[spatial:Feature[@rdf:about="' || $decodedUri || '"]]'):)
(:     let $placeRdf :=  $spatiumStructor:place-collection//spatial:Feature[@rdf:about= $decodedUri]:)

let $isMadeOfUris := if($placeRdf//spatial:Pi) then <uris>{ $placeRdf//spatial:Pi }</uris> else <none/>
     let $isPartOfUris := if($placeRdf//spatial:P) then <uris> { $placeRdf//spatial:P }</uris> else <none/>
     let $isInVicinityOfUris := if($placeRdf//spatial:C[@type='isInVicinityOf']) then <uris> { $placeRdf//spatial:C[@type='isInVicinityOf'] }</uris> else <none/>
     let $hasInItsVicinityUris:= if($placeRdf//spatial:C[@type='hasInItsVicinity']) then <uris> { $placeRdf//spatial:C[@type='hasInItsVicinity'] }</uris> else <none/>
     let $placeName := if($placeRdf//dcterms:title[@xml:lang=$placeRecordGenerator:lang]) then $placeRdf//dcterms:title[@xml:lang=$placeRecordGenerator:lang]/text()
        else $placeRdf//dcterms:title[1]/text()
     let $relatedDocuments := spatiumStructor:relatedDocuments($uriShort, $spatiumStructor:project)
     let $docs :=
                    <div id="relatedDocs">
                      <div class="xmlElementGroup">
                         <span class="subSectionTitle">Documents linked to this place</span>
                         <div id="listOfDocuments">
                         {if ($relatedDocuments) then
                         (
                         <ul class="listNoBullet">{
                           for $doc in $relatedDocuments
(:                           $spatiumStructor:doc-collection//tei:TEI[descendant-or-self::tei:placeName[@ref=$uriShort]]:)
                            (:$spatiumStructor:doc-collection//tei:TEI[tei:listPlace//tei:place//tei:placeName[@ref=$uriShort]]:)
                                   let $docId := data($doc/@xml:id)
                                   let $title := $doc//tei:titleStmt/tei:title/text()
                                   let $docUri := if($doc//tei:publicationStmt/tei:idno[@type="uri"]) then $doc//tei:publicationStmt/tei:idno[@type="uri"]
                                                            else $spatiumStructor:uriBase || "/artefacts/" || $docId
                                   let $placeType := (
                                                                let $placeNodes := 
                                                                $doc//tei:listPlace//tei:place//tei:placeName[matches(./@ref, $uriShort)]
                                                                for $placeNode in $placeNodes
                                                                    return
                                                                        if ($placeNode/@ana)
                                                                        then data($placeNode/@ana)
                                                                        else if (not($placeNode/@ana)) then ($placeNode/parent::node()/name())
                                                                        else ()
                                                                        )
                                             return
                                                <li>
                                                <span class="glyphicon glyphicon-file"/><a href="{ $docUri }" title="Open document { $docUri }" target="_self">{ $title }</a>
                                                
                                                                <span>[{ $placeType }]</span><a href="{ $docUri }" title="Open document { $docUri } in a new window" target="_blank">
                                                                <i class="glyphicon glyphicon-new-window"/></a>
                                                </li>
                            
                            }</ul>)
                            
                            else (<em>None</em>)}
                            </div>
                            
                     </div>
                </div>
   
   
   
   return
   if($placeRdf) then (
   <div>
   <!--<script type="text/javascript" src="$ausohnum-lib/resources/scripts/spatiumStructor/spatiumStructor.js"/>
   -->
     <!--
     <link href="$ausohnum-lib/resources/css/spatiumStructor.css" rel="stylesheet" type="text/css"/>
     -->
<!--
<link rel="stylesheet" href="$ausohnum-lib/resources/css/teiEditor.css"/>
-->

   <h2 id="resourceTitle">{ $placeName }</h2>
   <span id="currentPlaceUri" class="hidden">{ $uriShort }</span>
   <span id="currentPlaceId" class="hidden">{ substring-after($uriShort, "/places/") }</span>
   <span id="placeEditorType" class="hidden">archaeoManager</span>
   <h5>URI { $uriShort } { spatiumStructor:copyValueToClipboardButton("uri", 1, $uriShort) }</h5>
       <ul class="nav nav-pills" id="pills-tab" role="tablist">
                            <li class="nav-item active">
                              <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#nav-metadata" role="tab" aria-controls="pills-home" aria-selected="false">{ ausohnumCommons:label("archaeo-general", "Place details") }</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="pills-dates-tab" data-toggle="pill" href="#nav-dates" role="tab" aria-controls="pills-dates" aria-selected="false">{ ausohnumCommons:label("archaeo-temp-attestations", "Temporal attestations")}</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="pills-biblio-tab" data-toggle="pill" href="#nav-biblio" role="tab" aria-controls="pills-biblio" aria-selected="false">{ ausohnumCommons:label("archaeo-biblio", "Biblio &amp; other resources") }</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="pills-docs-tab" data-toggle="pill" href="#nav-docs" role="tab" aria-controls="pills-docs" aria-selected="false">{ ausohnumCommons:label("archaeo-documents", "Documents") }</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#nav-xmlfile" role="tab" aria-controls="pills-profile" aria-selected="false">XML</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade in active" id="nav-metadata" role="tabpanel" aria-labelledby="nav-metadata-tab">
                        
                                   { spatiumStructor:displayElement('title', $decodedUri, (), ()) }
                                    
                                    { spatiumStructor:displayElement('altLabel', $decodedUri, (), ()) }
                                  
                                    { spatiumStructor:displayElement('hasFeatureTypeMain', $decodedUri, (), ()) }
                                    
                                  
                                   {spatiumStructor:placeLocation($decodedUri)}
                                   {spatiumStructor:displayElement('exactMatch', $decodedUri, (), ())}
                                   <h3 style="margin-top: 10px!important;">{ ausohnumCommons:label("archaeo-links", "Link(s) with other places") }</h3>
                                   { spatiumStructor:isMadeOfArchaeo($uri, $isMadeOfUris)}
                                      { spatiumStructor:isPartOf($uri, $isPartOfUris)}
                                      { spatiumStructor:placeConnectedWith($uri, "isInVicinityOf", $isInVicinityOfUris)}
                                   { spatiumStructor:placeConnectedWith($uri, "hasInItsVicinity", $hasInItsVicinityUris)}
                                  
                                  
                                  { spatiumStructor:displayElement('generalCommentary', $decodedUri, (), ()) }
                                  { spatiumStructor:displayElement('privateCommentary', $decodedUri, (), ()) }
                                  
                           
                              {spatiumStructor:relatedPeople($uriShort)}
                              
                           
                            </div>
                           
                           <div class="tab-pane fade in" id="nav-dates" role="tabpanel" aria-labelledby="nav-dates-tab">
                           { spatiumStructor:displayElement('hasDateRangeGroup', $decodedUri, (), ()) }
                            </div>
                           
                           <div class="tab-pane fade in" id="nav-biblio" role="tabpanel" aria-labelledby="nav-biblio-tab">
                           <h3 style="margin-top: 10px!important;">Bibliography and other resources</h3>
                                    
                                   {""
(:                                   spatiumStructor:biblioManager($uriShort):)
                                   }
                                  {
                                   spatiumStructor:resourcesManager('seeFurther', $uriShort)
                                   }
                                   {
                                   spatiumStructor:resourcesManager('illustration', $uriShort)
                                   }
                                   
                                   { ""
(:                                   spatiumStructor:displayResourceList('illustration', $decodedUri) :)
                                   }
                                   {"" 
(:                                   spatiumStructor:displayElement('productionType', $decodedUri, (), ()) :)
                                   }
                                   
                                   
                           
                           
                           </div>
                           
                           <div class="tab-pane fade in" id="nav-docs" role="tabpanel" aria-labelledby="nav-docs-tab">
                           <button id="addNewDocToPlaceButton" class="btn btn-sm btn-primary pull-right" appearance="minimal" type="button" 
                           onclick="openDialog('dialogAddNewDocumentToPlace')" style="margin: 0 5px 0 5px;"><i class="glyphicon glyphicon-plus"></i></button>
                            { $docs}
                           </div>
                        
                        <div class="tab-pane fade in" id="nav-xmlfile" role="tabpanel" aria-labelledby="nav-text-tab">
                             { spatiumStructor:xmlFileEditorWithResourceAndUri($placeRdf, $decodedUri) }
                                                 
                         </div>
                         </div>
                         <script>console.log("Editor required");
var editor4File = ace.edit("xml-editor-file");
      editor4File.session.setMode("ace/mode/xml");
      editor4File.setOptions({{
            minLines: 40,
            maxLines: Infinity}});
            
 function getXmlEditorContent(){{
        var xmlFileEditor = ace.edit("xml-editor-file");
        return xmlFileEditor.getValue();
         
 }};           
</script>
   </div>)
   else
   ( <div class="jumbotron jumbotron-fluid">
                        <div class="container">
                          <h1 class="display-4">Error!!!</h1>
                          <p class="lead">There is no place with URI { $uriShort } decoded : { $decodedUri } in {$project-place-collection-path}</p>
                          { $placeRdf }
                        </div>
                      </div>)
 };

 declare function  placeRecordGenerator:newPlace(){

  <div>

       <div id="spatiumStructor" class="">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                <h3>{ ausohnumCommons:label("places-new-title", "Create a new place") }</h3>
                <a onclick="openDialog('dialogSearchPleiades')" class="newDocButton pull-right"><i class="glyphicon glyphicon-plus"/>{ ausohnumCommons:label("place-new-importPleiades", "Import a place from Pleiades dataset") }</a>
                <br/>

               <div class="form-group" style="padding-let: 1em;">
                        <label for="newPlaceStandardizedNameEn">{ ausohnumCommons:label("places-new-std-name-en", "Standardized name (in English)") }</label>
                                     <input type="text" class="form-control"
                                     id="newPlaceStandardizedNameEn" 
                                     name="newPlaceStandardizedNameEn"
                                     />
               </div>
                <h4>{ ausohnumCommons:label("places-new-name-otherLangs", "Name in another languages") }</h4>
                            
                        <div class="sectionInPanel">
                             <h5></h5>
                            <div style="margin-left: 1em!important;">
                             {skosThesau:dropDownThesau("c21856", "en", 'Language', 'inline', (), (), "xml")}       
                                    <div class="input-group">
                                        <span class="input-group-addon" od="prefLabelExtraValueLabel">{ ausohnumCommons:label("place-new-name", "Name") }</span>
                                        <input id="prefLabelExtraValue" name="prefLabelExtraValue" type="text" class="form-control" placeholder="Value" aria-describedby="prefLabelExtraValueLabel" />
                                </div>
                            </div>
                        </div>
               <div id="altLabelImport"/>
               <br/>
               <h4>{ ausohnumCommons:label("places-new-placeType", "Type of place") }</h4>
               
               {skosThesau:dropDownThesauForElement("hasFeatureTypeMain", substring-after($spatiumStructor:appVariables//placeMainTypeUri/text(), "/concept/"), $placeRecordGenerator:lang, ausohnumCommons:label("place-new-placeType", "Type of place"), 'inline', (), (), "uri")}
               
               {""(:skosThesau:dropDownThesauForElement("hasFeatureTypeMain", "c23473", "en", 'Place type', 'row', (), (), "uri"):)}
                {"" 
(:                skosThesau:dropDownThesauForElement("productionType", "c21879", "en", 'Production type', 'row', (), (), "uri"):)
                }
                
                <h4>Exact match(es)</h4>
                     <div class="form-group">
                        <label for="exactMatch">exactMatch</label>
                                     <input type="text" class="form-control"
                                     id="exactMatch" 
                                     name="exactMatch"
                                     />
               </div>
               <div id="exactMatchImport"/>
             <h4>{ ausohnumCommons:label("places-new-geometry", "Geometry") }</h4>
               <p>{ ausohnumCommons:label("places-new-geometry-msg", "You may use the 'Drawing tools' available on the map to create coordinates (best tool to create a point: ") }<img src="$ausohnum-lib/resources/scripts/spatiumStructor/markers/marker-grey.png" height="18px"/>)</p>             
                 <div class="input-group">
                                <span class="input-group-addon" id="geometryTypeLabel">Type</span>
                                <input id="geometryType" name="geometryType" type="text" class="form-control" placeholder="Geometry type" aria-describedby="geometryTypeLabel" />
                            </div>
                
                <div class="input-group">
                                <span class="input-group-addon" id="latNewPlaceLabel">Lat.</span>
                                <input id="latNewPlace" name="latNewPlace" type="text" class="form-control" placeholder="Latitude" aria-describedby="latNewPlaceLabel" />
                            </div>
                 <div class="input-group">
                                <span class="input-group-addon" id="longNewPlaceLabel">Long.</span>
                                <input id="longNewPlace" name="longNewPlace" type="text" class="form-control" placeholder="Longitude" aria-describedby="longNewPlaceLabel" />
                            </div>
                 <div class="input-group">
                                <span class="input-group-addon" id="polygonCoordinatesNewPlaceLabel">{ ausohnumCommons:label("place-new-geometry-coordinates", "Coordinates") }</span>
                                <input id="polygonCoordinatesNewPlace" name="longNewPlace" type="text" class="form-control" placeholder="Coordinates" aria-describedby="polygonCoordinatesNewPlaceLabel" />
                            </div>
                
                </div>
                
                <div>
                <br/>
                    <button id="createNewPlace" 
                                  class="btn btn-success"
                                  onclick="createNewPlaceLead('archaeo-manager')"
                                  appearance="minimal"
                                  type="button">{ ausohnumCommons:label("places-new-createPlace", "Create place") }<i class="glyphicon glyphicon glyphicon-saved"></i></button>
                 </div>               
                </div>
                </div>
              
                </div>
};