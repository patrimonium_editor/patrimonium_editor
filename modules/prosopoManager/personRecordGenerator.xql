xquery version "3.1";

module namespace personRecordGenerator = "http://ausonius.huma-num.fr/personRecordGenerator";

import module namespace console="http://exist-db.org/xquery/console";
import module namespace functx="http://www.functx.com";
import module namespace http="http://expath.org/ns/http-client";

import module namespace ausohnumCommons="http://ausonius.huma-num.fr/commons"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/commons/commonsApp.xql";
import module namespace prosopoManager="http://ausonius.huma-num.fr/prosopoManager"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/prosopoManager/prosopoManager.xql";

declare namespace apc = "http://patrimonium.huma-num.fr/onto#";
declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace ev = "http://www.w3.org/2001/xml-events";
declare namespace local = "local";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace foaf = "http://xmlns.com/foaf/0.1/";
declare namespace geo = "http://www.w3.org/2003/01/geo/wgs84_pos#";
declare namespace lawd="http://lawd.info/ontology/";
declare namespace pleiades="https://pleiades.stoa.org/places/vocab#";
declare namespace spatial="http://geovocab.org/spatial#";

declare variable $personRecordGenerator:lang := request:get-parameter("lang", ());

declare function personRecordGenerator:recordForDisplay($personUriShort as xs:string){
    let $currentUser := sm:id()//sm:real/sm:username/string()
    let $groups := string-join(sm:get-user-groups($currentUser), ' ')
    let $userRights :=
            if (matches($groups, ('sandbox'))) then "sandbox"
            else if(matches($groups, ('patrimonium'))) then "editor"
            else ("guest")
    let $personUriLong := $personUriShort || "#this"
    let $personId := functx:substring-after-last($personUriShort, "/")

return
if($prosopoManager:peopleCollection//lawd:person[@rdf:about= $personUriLong]) then (
          let $personRecord :=  $prosopoManager:peopleCollection//lawd:person[@rdf:about= $personUriLong]
          let $updateTitleWindow :=
                        if ($personId != "") then '$(document).ready( function () {{
                document.title = "Person ' || $personId || ' - ' || $personRecord//lawd:personalName[@xml:lang= $personRecordGenerator:lang]/text() || '";
                    }});' 
                    else '$(document).ready( function () {{
                document.title = "' || "APC Places" || '";
                    }});'

          return
<div class="row">
    <div id="personRecord" class="col-xs-7 col-sm-7 col-md-7">
    <h3 id="resourceTitle"><span>{ $personId }.</span>{ ausohnumCommons:displayElement('standardizedName', (), 'inLinePlainText', (), ()) }</h3>
                    <span id="currentPlaceUri" class="hidden">{ $personUriShort }</span>
                    <h4>URI { $personUriShort } {ausohnumCommons:copyValueToClipboardButton("uri", 1, $personUriShort)}</h4>
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                         <li class="nav-item active">
                                           <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#nav-metadata" role="tab" aria-controls="pills-home" aria-selected="false">Place details</a>
                                         </li>
                                         
                                         <li class="nav-item">
                                           <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#nav-xmlfile" role="tab" aria-controls="pills-profile" aria-selected="false">XML</a>
                                         </li>
                 </ul>
                 <div class="tab-content" id="nav-tabContent">
                       <div class="tab-pane fade in active" id="nav-metadata" role="tabpanel" aria-labelledby="nav-metadata-tab">
                                  { ausohnumCommons:displayElement('sex', (), 'simple', (), ()) }
                                  { ausohnumCommons:displayElement('personalStatus', (), 'simple', (), ()) }
                                  { ausohnumCommons:displayElement('socialStatus', (), 'simple', (), ()) }
                                  { ausohnumCommons:displayElement('juridicalStatus', (), 'simple', (), ()) }
                                  { ausohnumCommons:temporalRangeAttestations($personUriShort) }
                                  { ausohnumCommons:relatedDocuments($personUriShort, "people") }
                                  { ausohnumCommons:relatedPeopleToPerson($personUriLong)}
                                  { ausohnumCommons:personFuntions($personUriLong)}
                                  { ausohnumCommons:biblioAndResourcesList($personRecord, "seeFurther") }
                                  { ausohnumCommons:displayElement('generalCommentary', (), 'simple', (), ()) }
                       </div>
                       <div class="tab-pane fade in" id="nav-xmlfile" role="tabpanel" aria-labelledby="nav-text-tab">
                                          { prosopoManager:xmlFileEditorWithUri($personUriLong) }
                       </div>
                </div>         
    </div>
    
    <div class="col-xs-5 col-sm-5 col-md-5">
        <!--<div id="editorMap"/>
        { ausohnumCommons:relatedPlacesToPeople($personUriLong) }-->
     </div>
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" integrity="sha512-07I2e+7D8p6he1SIM+1twR5TIrhUQn9+I6yjqD53JQjFiMf8EtC93ty0/5vJTZGF8aAocvHYNEDJajGdNx1IsQ==" crossorigin="" />
        <script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet-src.js" integrity="sha512-WXoSHqw/t26DszhdMhOXOkI7qCiv5QWXhH9R7CgvgZMHz1ImlkVQ3uNsiQKu5wwbbxtPzFXd1hK4tzno2VqhpA==" crossorigin=""></script>
        <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet-providers.js"></script>
        <link rel="stylesheet" href="$ausohnum-lib-dev/resources/scripts/spatiumStructor/leaflet-search/src/leaflet-search.css"/>
         <script src="$ausohnum-lib/resources/scripts/spatiumStructor/leaflet-search/src/leaflet-search.js"></script>
        <link href='$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.fullscreen/leaflet.fullscreen.css' rel='stylesheet' />
        <script src='$ausohnum-lib/resources/scripts/spatiumStructor/leaflet.fullscreen/Leaflet.fullscreen.min.js'></script>
       <!--Markercluster -->
       <link rel="stylesheet" href="https://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.css" />
       <link rel="stylesheet" href="https://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.Default.css" />
       <script src="https://leaflet.github.io/Leaflet.markercluster/dist/leaflet.markercluster-src.js"></script>
        <script type="text/javascript" src="$ausohnum-lib/resources/scripts/spatiumStructor/L.Control.Geonames.min.js"/>

        <!--<link href="$ausohnum-lib/resources/css/spatiumStructor.css" rel="stylesheet" type="text/css"/>-->
        
        <script type="text/javascript" src="$ausohnum-lib/resources/scripts/spatiumStructor/spatiumStructor.js"/>
        
  
        <script type="text/javascript">{ $updateTitleWindow }
          
        </script>

    
</div>                
            )
            else()

};
declare function personRecordGenerator:recordForEditing($personUri as xs:string){
let $personUriLong := if(contains($personUri, "this")) then  $personUri else $personUri || "#this"
let $personUriShort := if(contains($personUri, "this")) then  substring-before($personUri, "#this") else $personUri

 return
  if(not($prosopoManager:peopleCollection//lawd:person[@rdf:about= $personUriLong]))
    then ("Aucun enregistement avec un URI " ||  $personUriShort )
  else
  (
    
    let $personRecord :=  $prosopoManager:peopleCollection//lawd:person[@rdf:about= $personUriLong]
    let $persName := $personRecord//lawd:personalName/text()
     let $docs := <div class="xmlElementGroup">
                         <h4 class="subSectionTitle">List of documents attached to this person</h4>
                         <div id="listOfDocuments">
                         <ul class="listNoBullet">{
                           for $doc at $pos in $prosopoManager:doc-collection//tei:TEI[descendant-or-self::tei:listPerson//tei:person[@corresp=$personUriShort]]
                            (:$spatiumStructor:doc-collection//tei:TEI[tei:listPlace//tei:place//tei:placeName[@ref=$personUriShort]]:)
                                   
                                   let $title := $doc//tei:titleStmt[1]/tei:title[not(ancestor::tei:bibFull)]/text()
                                   let $docUri := $doc//tei:fileDesc/tei:publicationStmt/tei:idno[@type="uri"]
                                   let $datingNotBefore :=
                                                    if($doc//tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origDate[1]/@notBefore-custom)
                                                           then data($doc//tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origDate[1]/@notBefore-custom)
                                                    else if($doc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origDate[1]/tei:date/@notBefore)
                                                    
                                                    then data($doc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origDate[1]/tei:date[1]/@notBefore)
                                                    else ()
                                    let $datingNotAfter :=
                                                    if($doc//tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origDate[1]/@notAfter-custom)
                                                           then data($doc//tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origDate[1]/@notAfter-custom)
                                                    else if($doc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origDate[1]/tei:date/@notAfter)
                                                    
                                                    then data($doc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origDate[1]/tei:date/@notAfter)
                                                    else ()
        
                                   let $placeType := (
                                                                let $placeNodes := 
                                                                $doc//node()[@ref=$personUriShort]
                                                                for $placeNode in $placeNodes
                                                                    return
                                                                        if ($placeNode/@ana)
                                                                        then data($placeNode/@ana)
                                                                        else ($placeNode/parent::node()/name())
                                                                        )
                                             return
                                                <li>{ $pos }
                                                <span class="glyphicon glyphicon-file"/><a href="{ $docUri }" title="Open this document in same window" target="_self">{ $title }</a>
                                                <a href="{ $docUri }" target="_blank"><i class="glyphicon glyphicon-new-window"/></a>
                                                <a href="{ replace($docUri, '/documents', '/egypt-documents')}" target="_blank" title="Open with editor dedicated to Egyptian material"><i class="glyphicon glyphicon-flash"/></a>                                                               
                                                               {if($datingNotBefore) then <span>[{$datingNotBefore }{
                                                               
                                                               if($datingNotAfter) then "-" || $datingNotAfter else ()}]</span>
                                                                else()
                                                                }
                                                </li>
                            
                            }</ul>
                          </div>
                     </div>
  
   return
   (
   
   <div id="htmlContent">
    <script src="$ausohnum-lib/resources/scripts/jquery/trumbowyg/trumbowyg.js"/>
    <script src="$ausohnum-lib/resources/scripts/jquery/trumbowyg/plugins/teitags/trumbowyg.teitags.js"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.27.3/ui/trumbowyg.min.css"/>
                      <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                              <span id="currentPeopleUri" class="hidden">{ $personUriShort }</span>
                                              <h3>{ $persName }</h3>
                                              <h4>URI { $personUriShort }</h4>
                                              <div class="">
                                                <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                            <li class="nav-item active">
                                                              <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#nav-metadata" role="tab" aria-controls="pills-home" aria-selected="false">Person details</a>
                                                            </li>
                                                            
                                                            <li class="nav-item">
                                                              <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#nav-xmlfile" role="tab" aria-controls="pills-profile" aria-selected="false">XML</a>
                                                            </li>
                                                 </ul>
                                                 <div class="tab-content" id="nav-tabContent">
                                                            <div class="tab-pane fade in active" id="nav-metadata" role="tabpanel" aria-labelledby="nav-metadata-tab">
                                                                         <div class="row">
                                                                         <div class="col-sm-4 col-md-4 col-lg-4">
                                                                             { prosopoManager:displayElement('standardizedName', $personUriShort, (), ()) }
                                                                             { prosopoManager:displayElement('exactMatches', $personUriShort, (), ()) }
                                                                             { prosopoManager:displayElement('sex', $personUriShort, (), ()) }
                                                                             
                                                                        </div>
                                                                        <div class="col-sm-4 col-md-4 col-lg-4">
                                                                                {prosopoManager:hasFunction($personUriLong)}
                                                                             
                                                                         </div>
                                                                        <div class="col-sm-4 col-md-4 col-lg-4">
                                                                                {prosopoManager:hasBond($personUriLong)}
                                                                                { $docs }
                                                                                 {prosopoManager:relatedPlaces( $personUriLong )}
                                                                        </div>
                                                                        </div>
                                                                        <div class="row">
                                                                        { prosopoManager:resourcesManager('seeFurther', $personUriShort) }
                                                                       
                                                                             { prosopoManager:displayElement('generalCommentary', $personUriShort, (), ()) }
                                                                             <br/>
                                                                             { prosopoManager:displayElement('privateCommentary', $personUriShort, (), ()) }
                                                                        
                                                                        </div>
                                                            
                                                            </div>
                                                            
                                                            <div class="tab-pane fade in" id="nav-xmlfile" role="tabpanel" aria-labelledby="nav-text-tab">
                                                             { prosopoManager:xmlFileEditorWithUri($personUriLong) }
                                                            </div>
                                                    </div>
                                        </div>
                                </div>
                            </div></div>
   )
  )
  };

  declare function personRecordGenerator:recordForEditingInDoc($project as xs:string?, $personId as xs:string){
    let $personUri := "https://lead.ifao.egnet.net/people/" || $personId 
let $personUriLong := if(contains($personUri, "this")) then  $personUri else $personUri || "#this"
let $personUriShort := if(contains($personUri, "this")) then  substring-before($personUri, "#this") else $personUri

 return
  if(not($prosopoManager:peopleCollection//lawd:person[@rdf:about= $personUriLong]))
    then ("Aucun enregistement avec un URI " ||  $personUriShort )
  else
  (
    
    let $personRecord :=  $prosopoManager:peopleCollection//lawd:person[@rdf:about= $personUriLong]
    let $persName := $personRecord//lawd:personalName/text()
    
  
   return
   (
   
   <div id="">
    <script src="$ausohnum-lib/resources/scripts/prosopoManager/prosopoManager.js" />

      <div class="">
              <div class="col-sm-12 col-md-12 col-lg-12">
                        <span id="currentPeopleUri" class="hidden">{ $personUriShort }</span>
                        <h3>{ $persName }</h3>
                        <h4>URI { $personUriShort }</h4>
                        <div class="">
                          <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                      <li class="nav-item active">
                                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#nav-metadata" role="tab" aria-controls="pills-home" aria-selected="false">Person details</a>
                                      </li>
                                      
                                      <li class="nav-item">
                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#nav-xmlfile" role="tab" aria-controls="pills-profile" aria-selected="false">XML</a>
                                      </li>
                            </ul>
                            <div class="tab-content" id="nav-tabContent">
                                      <div class="tab-pane fade in active" id="nav-metadata" role="tabpanel" aria-labelledby="nav-metadata-tab">
                                            <div class="row">
                                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                                        { prosopoManager:displayElement('standardizedNameFr', $personUriShort, (), ()) }
                                                        { prosopoManager:displayElement('altNameFr', $personUriShort, (), ()) }
                                                        { prosopoManager:displayElement('nicknameFr', $personUriShort, (), ()) }
                                                        { prosopoManager:displayElement('exactMatches', $personUriShort, (), ()) }
                                                        { prosopoManager:displayElement('sex', $personUriShort, (), ()) }
                                                        
                                                  </div>
                                                  <div class="col-sm-4 col-md-4 col-lg-4">
                                                          {prosopoManager:hasFunction($personUriLong)}
                                                        { prosopoManager:displayElement('floruitNotBefore', $personUriShort, (), ()) }
                                                        { prosopoManager:displayElement('floruitNotAfter', $personUriShort, (), ()) }
                                                        { prosopoManager:displayElement('floruitComment', $personUriShort, (), ()) }

                                                    </div>
                                                  <div class="col-sm-4 col-md-4 col-lg-4">
                                                          {prosopoManager:hasBond($personUriLong)}
                                                      
                                                            
                                                  </div>
                                                  </div>
                                                  <div class="row">
                                                  { prosopoManager:resourcesManager('seeFurther', $personUriShort) }
                                                  
                                                        { prosopoManager:displayElement('generalCommentary', $personUriShort, (), ()) }
                                                        <br/>
                                                        { prosopoManager:displayElement('privateCommentary', $personUriShort, (), ()) }
                                                  
                                                  </div>
                                      
                                      </div>
                                      
                                      <div class="tab-pane fade in" id="nav-xmlfile" role="tabpanel" aria-labelledby="nav-text-tab">
                                        { prosopoManager:xmlFileEditorWithUri($personUriLong) }
                                      </div>
                              </div>
                  </div>
          </div>
                            </div></div>
   )
  )
  };