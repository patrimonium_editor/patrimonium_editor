xquery version "3.1";

import module namespace app="https://ausohnum.huma-num.fr/apps/patrimonium_editor/templates" at "../app.xql";


import module namespace ausohnumCommons="http://ausonius.huma-num.fr/commons"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/commons/commonsApp.xql";

import module namespace functx="http://www.functx.com";

import module namespace prosopoManager="http://ausonius.huma-num.fr/prosopoManager"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/prosopoManager/prosopoManager.xql";

import module namespace personRecordGenerator ="http://ausonius.huma-num.fr/personRecordGenerator"
      at "./personRecordGenerator.xql";

declare namespace apc = "http://patrimonium.huma-num.fr/onto#";
declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace ev = "http://www.w3.org/2001/xml-events";
declare namespace local = "local";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace foaf = "http://xmlns.com/foaf/0.1/";
declare namespace geo = "http://www.w3.org/2003/01/geo/wgs84_pos#";
declare namespace lawd="http://lawd.info/ontology/";
declare namespace pleiades="https://pleiades.stoa.org/places/vocab#";
declare namespace spatial="http://geovocab.org/spatial#";
declare namespace snap="http://onto.snapdrgn.net/snap#";

let $origin:= request:get-parameter("origin", "")
let $project:= request:get-parameter("project", "")

let $peopleCollection := util:eval('collection("' || $prosopoManager:data-collection-path || "/people" || '")')
let $peopleNumber := count($peopleCollection//lawd:person)
(:let $peopleNumber := count($prosopoManager:peopleCollection//lawd:person):)
let $nameNumber := count($peopleCollection//lawd:personalName)
let $bondNumber := count($peopleCollection//snap:hasBond)
let $peopleId := request:get-parameter("resource", "")
let $peopleUri := $prosopoManager:uriBase || "/people/" || $peopleId || "#this"
(:let $personRecord := $prosopoManager:peopleRepo//lawd:person[@rdf:about = $peopleUri]:)
let $updateTitleWindow :=
                        if ($peopleId != "") then '$(document).ready( function () {{
                document.title = "People - " + "' || $peopleId || '";
                    }});' 
                    else '$(document).ready( function () {{
                document.title = "' || $project || " People" || '";
                    }});'
return

if($origin = "call") then
    personRecordGenerator:recordForEditing($peopleUri)
    else 

 <div data-template="templates:surround" data-template-with="templates/page.html" data-template-at="content">
    <script src="$ausohnum-lib/resources/scripts/jquery/trumbowyg/trumbowyg.js"/>
    <script src="$ausohnum-lib/resources/scripts/jquery/trumbowyg/plugins/teitags/trumbowyg.teitags.js"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.27.3/ui/trumbowyg.min.css"/>
      
        <div id="prosopoManager" class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div id="sideColPeople" class="col-xs-3 col-sm-3 col-md-3">
                        <button class="btn btn-sm btn-primary" onclick="openNewPersonForm()">Create a new person</button>
                        <br/>
                            { app:listPeopleAsTable() }
<!--                        <ul class="nav nav-tabs" id="peopleTab" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" id="home-peopleTab" data-toggle="tab" href="#homePeopleTab" role="tab" aria-controls="home-peopleTab" aria-selected="true">Search People</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" id="allPeopleTree-peopleTab" data-toggle="tab" href="#allPeopleTree" role="tab" aria-controls="allPeopleTree-peopleTab" aria-selected="false">All People</a>
                                </li>
                                
                         </ul>
                         <div class="tab-content" id="peopleTabContent">
                                <div class="tab-pane fade show active in" id="homePeopleTab" role="tabpanel" aria-labelledby="home-peopleTab">
                                       <input name="searchPeople" id="searchPeople" class="projectPeopleSearch" placeholder="Search among { $peopleNumber } persons" title="" autocomplete="off">
                                       <span id="LoadingImage" style="display: none">
                                       <img class="loadingImage" src="$shared/resources/scripts/jquery/skin/loading.gif" /></span></input>
                                        <div id="peopleSearchResult"/>
                                
                                </div>
                                <div class="tab-pane fade" id="allPeopleTree" role="tabpanel" aria-labelledby="allPeopleTree-peopleTab">
                                
                                    { prosopoManager:listPeopleAsTable() }
                                </div>
                                
                         </div>
 -->                       
                        </div>
                        
                        <div id="peopleEditor" class="col-xs-9 col-sm-9 col-md-9">
                        { if($peopleId ="") then (
                        <div>
                        <h1 class="display-4">{ $project } - People Manager</h1>
                        <h4>Total of...</h4>
                        <ul class="list-group">
                        <li class="list-group-item"><strong>... persons: </strong>{$peopleNumber}</li>
                        <li class="list-group-item"><strong>... proper names: </strong>{$nameNumber}</li>
                        <li class="list-group-item"><strong>... relationships:</strong> {$bondNumber}</li>
                        </ul>
                        </div>
                        )
                        else if($peopleId ="new") then
                        prosopoManager:newPersonForm()
                        else personRecordGenerator:recordForEditing($peopleUri) }
                        </div>
                    </div>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5">

            
                </div>
            </div><!--End of row-->
<!--    <div id="mapid"></div>-->
            
            { prosopoManager:searchProjectPeopleModal() }
            { prosopoManager:addFunctionModal() }
            { prosopoManager:deleteRelationshipModal() }
            { prosopoManager:addResourceDialog("seeFurther") }
            </div>



        <link href="$ausohnum-lib/resources/css/prosopoManager.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="$ausohnum-lib/resources/scripts/prosopoManager/prosopoManager.js"/>
          <script type="text/javascript">{ $updateTitleWindow }</script>


 </div>
